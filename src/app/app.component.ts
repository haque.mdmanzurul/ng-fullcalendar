import * as jQuery from 'jquery';
(window as any).jQuery = (window as any).$ = jQuery; // This is needed to resolve issue.
import { Component, OnInit } from '@angular/core';
//import 'fullcalendar';
import 'fullcalendar-scheduler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'My Calendar';

  ngOnInit() {
	$('#calendar').fullCalendar({});

  	$('#calendar2').fullCalendar({
  	  header: {center: 'month,timelineFourDays'},
	  views: {
	    timelineFourDays: {
	      type: 'timeline',
	      duration: { days: 4 }
	    }
	  }
	});
  }
}
